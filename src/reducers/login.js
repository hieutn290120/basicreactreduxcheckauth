
const initLogin = {
    list: [],
    token: false
}

const loginReducer = (state = initLogin,action) => {
    switch (action.type) {
        case 'LOGIN':
            const newList = [...state.list];
            newList.push(action.payload);
            return {
                ...state,
                list:newList,
                token: true
            }; 
        case 'LOGOUT':
            return {
               ...state,
               list:[],
               token:false
            }   
        default:
            return state  
    }
    
}



export default loginReducer