import  hobbyReducer  from "./hobby";
import  userRedecer  from "./user";
import loginReducer from "./login"
import { combineReducers } from "redux";

const rootReducer = combineReducers({
    hobby:hobbyReducer,
    user:userRedecer,
    checklogin:loginReducer
})


export default rootReducer;