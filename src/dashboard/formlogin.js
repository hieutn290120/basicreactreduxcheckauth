import React from 'react'
import Login from '../components/Login'
import {BrowserRouter as Router, Route, Redirect,Link  } from "react-router-dom";
import { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button'
import imgageindex from './pexels-photo-3422964.jpeg'
import Dashboard from './index'
import { useDispatch } from 'react-redux';
import {LoginAction} from '../action/login'





function FormLogin() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const [arr,setArr] = useState({});
  const [checked,setLogin] = useState(false);
  const usedispatch = useDispatch();
  // Note: the empty deps array [] means
  // this useEffect will run once
  // similar to componentDidMount()

  // Lưu ý: mảng nhiệm vụ trống [] có nghĩa là
  // useEffect này sẽ chạy một lần
  // tương tự với componentDidMount ()

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then(res =>  res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setItems(result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        
        //Lưu ý: điều quan trọng là phải xử lý lỗi ở đây
        // thay vì một khối catch () để chúng ta không nuốt
        // ngoại lệ từ các lỗi thực tế trong các thành phần
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [isLoaded]);
  //xử lý xự kiện button click
  const login=()=>{
    setIsLoaded(!isLoaded);
    let data = JSON.parse(localStorage.getItem('User'));
    let account = {
      email:arr['email'],
      password:arr['password']
    }
    if(data){
      if(arr['email'] === data['email'] && arr['password'] === data['password']){
        const action = LoginAction(account);
        alert('Đăng nhập thành công');
        setLogin(true)
        usedispatch(action);
      }
    }else{
      setIsLoaded(true)
      alert('Đang nhập thât bại, sai tên Tài Khoản hoặc Mật Khẩu');
    }
    
  }
  var data = <div className="App2">
                  <h1>Login</h1>
            <div style={{height:'50px', textAlign:'center'}}>
                <img src={imgageindex} style={{width:'500px'}}></img>
            </div>
                <form className="form2" style={{backgroundColor:'rgba(75, 109, 100, 0)'}}>
                        <Login getValue={getValue} name="email" label="Địa chỉ Email" type="text" paclehoder="Điền email vào đây"/>
                        <Login getValue={getValue} name="password" label="Mật Khẩu" type="password" />
                        <Button variant="outlined" color="primary" onClick={login} >
                        Login
                        </Button>
                </form>
            </div>
  // Xử lý sự kiện lấy value của thẻ input 
  function getValue(value,name) {
    var obj={};
    obj[name]=value;
    var result = Object.assign({},arr,obj)
    setArr(result)
  }
//render
  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
        <div className="App">
            <Route exact path="/">
              {checked ? <Redirect to="/products" /> : data}
            </Route>
        </div>
      );    
  }
}
export default FormLogin;   