import React from 'react'
import Person from '../components/Persson1'
import { BrowserRouter as Router, Route, Redirect, Link } from "react-router-dom";
import { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button'
import imgageindex from './pexels-photo-3422964.jpeg'
import Login from './formlogin'


function FormRegister() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const [arr, setArr] = useState({});
  const [validate, setValidate] = useState([]);
  const [checked, setRegister] = useState(false);
  // const [errPass, setErrPass] = useState(null);
  // const [errPass2, setErrPass2] = useState(null);
  // const [errName, setErrName] = useState(null);
  // const [errPhone, setErrPhone] = useState(null);


  // Note: the empty deps array [] means
  // this useEffect will run once
  // similar to componentDidMount()

  // Lưu ý: mảng nhiệm vụ trống [] có nghĩa là
  // useEffect này sẽ chạy một lần
  // tương tự với componentDidMount ()

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setItems(result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.

        //Lưu ý: điều quan trọng là phải xử lý lỗi ở đây
        // thay vì một khối catch () để chúng ta không nuốt
        // ngoại lệ từ các lỗi thực tế trong các thành phần
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [isLoaded]);
  //xử lý xự kiện button click đăng kí
  const register = () => {
    setIsLoaded(!isLoaded);
    var re = [];
    if (!arr['email']) {
      re['email'] = { 'email': 'Không được để trống' }
      setValidate(re);
    } if (!arr['password']) {
      re['password'] = { 'password': 'Không được để trống' }
      setValidate(re);
    } if (!arr['password2']) {
      re['password2'] = { 'password2': 'Không được để trống' }
      setValidate(re);
    }
    if (!arr['name']) {
      re['name'] = { 'name': 'Không được để trống' }
      setValidate(re);
    }
    if (!arr['phone']) {
      re['phone'] = { 'phone': 'Không được để trống' }
      setValidate(re);
    }
    if (typeof arr['name'] !== 'undefined') {
      if (!arr['name'].match(/^\D./)) {
        re['name'] = { 'name': 'Phải bắt đầu bằng chữ cái!' }
        setValidate(re);
      }
    }
    if (typeof arr['phone'] !== 'undefined') {
      if (!arr['phone'].match(/^\d{10}$/)) {
        re['phone'] = { 'phone': 'Số điện thoại chỉ được nhập 10 số !' }
        setValidate(re);
      }
    }
    if (typeof arr['email'] !== "undefined") {
      let lastAcongPost = arr['email'].lastIndexOf('@');
      let lastDauchamPost = arr['email'].lastIndexOf('.');
      if (!(lastAcongPost < lastDauchamPost && lastAcongPost > 0 && arr['email'].indexOf('@@') == -1 && lastDauchamPost > 2 && (arr['email'].length - lastDauchamPost) > 2)) {
        re['email'] = { 'email': 'Email không đúng định dạng, (xyz@gmail.com)' }
        setValidate(re);
      }
    }
    if (arr['password'] !== arr['password2']) {
      re['password2'] = { 'password2': 'Password phải giống nhau' }
      setValidate(re);
    }
    if (arr['email'] && arr['password'] && arr['password'] === arr['password2'] && arr['password2'] && arr['phone'] && arr['name']) {
      localStorage.setItem('User', JSON.stringify(arr));
      setRegister(true);
      alert('Đăng kí tài khoản thành công!');
    } else {
      alert('Đăng kí tài khoản không thành công!');
      setRegister(false);
      setIsLoaded(false);
    }
  }
  // Xử lý sự kiện lấy value của thẻ input 
  function getValue(value, name) {
    var obj = {};
    obj[name] = value;
    var result = Object.assign({}, arr, obj)
    setArr(result)
  }
  var data = <div className="App2">
    <div>
      <img src={imgageindex} style={{ width: '500px', height: '200px' }}></img>
    </div>
    <form className="form1">
      <Person getValue={getValue} name="email" label="Địa chỉ Email" type="text" span={validate['email'] ? validate['email'].email : ''} />
      <Person getValue={getValue} name="password" label="Mật Khẩu" type="password" span={validate['password'] ? validate['password'].password : ''} />
      <Person getValue={getValue} name="password2" label="Nhập Lại Mật Khẩu" type="password" span={validate['password2'] ? validate['password2'].password2 : ''} />
      <Person getValue={getValue} name="phone" label="Số Điện Thoại" type="text" span={validate['phone'] ? validate['phone'].phone : ''} />
      <Person getValue={getValue} name="name" label="Dia Chi" type="text" span={validate['name'] ? validate['name'].name : ''} />
      <Button variant="outlined" color="primary" onClick={register} >
        Register
      </Button>
    </form>
  </div>
  //render
  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <div className="App">
        <h1>Register</h1>
        <Route exact path="/register">
          {checked ? <Redirect to="/" /> : data}
        </Route>
      </div>
    );
  }
}
export default FormRegister;   