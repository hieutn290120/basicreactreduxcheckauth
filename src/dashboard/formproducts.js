import React from 'react'
import Products from '../products/products'
import { BrowserRouter as Router, Route, Redirect, Link } from "react-router-dom";
import { useEffect, useState } from 'react';
import Button from '@material-ui/core/Button'
import Headers from '../dashboard/header'
import Grid from '@material-ui/core/Grid'
import Cart from '../cart/cart'

function FormProducts() {
    const [obj,setObj] = useState({});
    const [isLoaded, setIsLoaded] = useState(false);
    const getValue = (name,price) => {
        const myobj = {};
        myobj[name] = {name:name, price:price}
        var data = Object.assign({}, obj, myobj)
        setObj(data);
    }
    const countCard = Object.keys(obj).length;
    localStorage.setItem('Cart',JSON.stringify(obj))
    return (
      <div className="App">  
        <Grid container spacing={2}>
                <Grid item xs={3}>
                    <Products getValue={getValue}  name="Macbook Air" price="1000"/>
                </Grid>
                <Grid item xs={3}>
                    <Products getValue={getValue} name="Dell" price="1200"/>
                </Grid>
                <Grid item xs={3}>
                    <Products getValue={getValue} name="SamSung" price="1100"/>
                </Grid>
                <Grid item xs={3}>
                    <Products getValue={getValue} name="LG" price="15000"/>
                </Grid>
        </Grid>
        <div>
           {countCard}
        </div>
    </div>
    );
}
export default FormProducts;   