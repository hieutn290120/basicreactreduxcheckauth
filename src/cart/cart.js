import React from 'react'
import { useEffect, useState } from 'react';
import { Input, TextField } from '@material-ui/core';

const Cart = cart => {
    const {name,price} = cart;
    return(
        <div>
           <h2>Name:{name}</h2>
           <h2>Price:{price}</h2>
        </div>
    )
}
export default Cart