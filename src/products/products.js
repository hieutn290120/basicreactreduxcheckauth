import React from 'react'
import Header from '../dashboard/header'
import formCart from '../dashboard/formcart'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
const Products = products => {
    const { getValue,name, price, ...other } = products;
    function handleValue(e) {
        getValue(name,price);
    }
    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },
    }));
    const classes = useStyles();
    return (
        <div>
            <Paper className={classes.paper} >
                <div style={{ textAlign: 'center' }}>
                    <h1>Name: {name}</h1>
                    <h3>Price : {price}$</h3>
                    {/* <button onClick={handleValue}>Buy</button> */}
                    <Button onClick={handleValue} variant="outlined" color="primary" >
                        Buy
                    </Button>
                </div>
            </Paper>

        </div>
    )
}
export default Products