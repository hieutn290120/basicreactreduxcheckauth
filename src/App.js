import { useEffect, useState } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, useHistory, Link, Redirect, Switch } from "react-router-dom";
import FormLogin from './dashboard/formlogin'
import FormRegister from './dashboard/formregister'
import Dashboard from './dashboard/formproducts'
import Cart from './dashboard/formcart'
import Button from '@material-ui/core/Button'
import Headers from './dashboard/header'
import FormCart from './dashboard/formcart'
import { useDispatch, useSelector } from 'react-redux';
import {LogoutAction} from './action/login'

function App() {

  let checkLogin = useSelector(state => state.checklogin.token);
  
  const usedispatch = useDispatch();
  const ClickLogout= () => {
      const action = LogoutAction();
      usedispatch(action);
  }
        var html1 = <Router  >
        <Redirect to="/products" />
        <Route exact path="/register">
          <FormRegister />
          <Link to="/">
            <Button variant="contained" color="primary">
              Đến trang Login!
            </Button>
          </Link>
        </Route>
        <Route path="/products" >
          <Dashboard />
          <Link to="/">
            <Button onClick={ClickLogout} variant="contained" color="primary">
              Logout!
            </Button>
          </Link>
          <Link to="/cartdetail">
            <Button variant="outlined" color="primary">
              CartDetail
            </Button>
          </Link>
        </Route>
        <Route exact path="/cartdetail" >
          <FormCart />
          <Link to="/products">
            <Button variant="contained" color="primary">
              Home!
            </Button>
          </Link>
        </Route>
      </Router>
      
  if(!checkLogin){
    var html2 = <Router >
       <Redirect to="/" />
              <Route exact path="/" >
                <h1>Bai Tap Dau Tien</h1>
                <FormLogin />
                <Link to="/register">
                  <Button variant="contained" color="primary">
                    Register!
                    </Button>
                </Link>
              </Route>
            </Router>
  }
  return (
    <div className="App">
      <Headers />
        {checkLogin ? html1 : html2 }
    </div>
  )
}

export default App;
