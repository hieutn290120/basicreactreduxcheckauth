import React from 'react'
import {useDispatch, useSelector} from 'react-redux'
import HobbyList from '../Home/HobbyList/index'
import { addNewHobby } from '../action/hobby';
HomePage.propTypes = {

};

function HomePage(props){
    const hobbyList = useSelector(state => state.hobby.list);
    const dispatch = useDispatch();
    const handleAddHobbyClick = () => {
        //Random a hobby objec : id + title
        const newId = 1000 + Math.trunc((Math.random() * 9000));
       
        const newHobby = {
            id: newId,
            title: `Hobby ${newId}`
        }
        //Dispatch action to add a new hobby to redux store

        const action = addNewHobby(newHobby);
        dispatch(action);
    }
    return(
        <div className="home-page">
            <h1>Bài Tập Thứ 2 - Redux in React</h1>
            <button onClick={handleAddHobbyClick}>Random Hobby</button>
            <HobbyList hobbyList={hobbyList} />
        </div>
    )
}
export default HomePage