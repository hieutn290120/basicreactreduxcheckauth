export const LoginAction = (login) =>{
    return {
        type: 'LOGIN',
        payload: login,
    }
}

export const LogoutAction = (login) => {
    return {
        type: 'LOGOUT',
        payload: login,
    }
}